
### Ezone

前言：希望所有阅读过本项目源代码的朋友，记得给个Star。

博客模板来自于“杨青”，传送门：http://www.yangqq.com/download/div/2014-04-17/661.html

1、博主博客：http://54sb.org

2、demo站：http://test.coody.org/

3、demo站后台：http://test.coody.org/admin 用户名：coody 密码：123456 （不用在意后缀）

4、前端功能包括：文章列表、文章类别、站长/博主信息、最新/最热文章、分页、友情链接、文章详情等。

5、后台功能包括：网站运行状态、资源管理、监听管理、缓存管理、后缀管理等。

6、集成debug平台，可用于预览、调试、监听、修改class/jar等信息与内容，实时生效。

7、可动态设置网站后缀。

8、自带爬虫系统，不需要发布文章。

9、版权说明：

    作者：Coody
    反馈邮箱：644556636@qq.com
    建议反馈/交流群：218481849

相关截图说明：

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112258_d2da2f3f_1200611.png "1.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112307_ba6dc63d_1200611.png "2.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112312_04062236_1200611.png "3.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112318_c57e764a_1200611.png "4.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112326_183e133a_1200611.png "5.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112334_3b07a94e_1200611.png "6.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112342_fce70fcb_1200611.png "7.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112348_8ddbd086_1200611.png "8.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112353_7480f4d3_1200611.png "9.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112359_c1bdc05b_1200611.png "10.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112406_e0ac0d1f_1200611.png "11.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0424/112412_99f53a17_1200611.png "12.png")